# MySQL with XtraBackup

The MySQL with XtraBackup Docker image is built on the [MySQL 5.7](https://hub.docker.com/r/mysql/mysql-server/) Docker image and contains the open source backup software [XtraBackup](https://www.percona.com/software/mysql-database/percona-xtrabackup) from Percona. XtraBackup performs non-blocking full and incremental backups, making it ideal for larger databases like the [WeatherDatabase](https://gitlab.com/ars-snow/WeatherDatabase). The backups are ran using [bgbackup](https://github.com/bstillman/bgbackup) which provides a clean interface for setting up and tracking database backups.

## Configuring the backups
`bgbackup` is included in the Dockerfile and is added to `/home/bgbackup` within the container. Open `bgbackup.cnf` in the reposistory and set the desired backup user and password along with the backup configurations.

The following configurations should be changed based on the implementation:
* `backupuser`: The MySQL user name to use for the backups
* `backuppass`: The MySQL user password for the backups
* `fullbackday`: The day of week for the full backups
* `differential`: `no` for incremental backups, `yes` for differential
* `keepnum`: number of full backups to keep in the `/backup` folder

Once `bgbackup` is configured, the `backupuser` must be added to the MySQL database. Change the user name in `sql/initialize_bgbackup.sql`, which will be ran when the image is ran.

## Starting the container
Backups are saved to a mounted volume with the container at `/backup` and should be shared with the host device. This will ensure that the backups can be transfered to another location. An example `docker-compose.yml` file is included with some options set and volumes mounted. Important volumes are:
* The MySQL data folder (`/var/lib/mysql`)
* The backup folder (`/backup`)

## Taking backups
Backups should be scheduled on the host machine. For example, with the `docker-compose.yml` image running in dameon mode, schedule a task to run the command:

```
docker exec -it mysqlxtra_mysql_1 /home/bgbackup/bgbackup.sh
```

Based on the `bgbackup` configuration, this will take a full or incremental backup and place that into the `/backup` folder.

## Restoring backups

XtraBackup creates both full and incremental backups. Each backup must be prepared before restoring to the MySQL data directory. Ensure that a copy of the backups are kept during restoring so that the preparing can be redone if an error occurs.

### Restoring to a full backup
To restore to a point where there was a full backup, the following steps are performed:

1. Open the bash on the running container
    ```
    docker exec -it mysqlxtra_mysql_1 bash
    ```
    
2. Prepare the full backup
    ```
    innobackupex --apply-log --redo-only /backup/full-{DATE} --user={backupuser} --password={backuppass}
    ```
    
3. Exit and stop the container, this will shut down the MySQL database
4. The MySQL data-dir is a persistant volume mounted on the local drive, remove the contents of the data-dir
5. `rsync` or `cp` the persistant backup volume mounted on the local drive to the local data-dir
6. Restart the container

### Restoring to an incremental backup
Incremental backups are built sequentially upon a full backup. Therefore, the full backup is prepared and the incremental backups are added to the prepared full backup.

1. Prepare the full backup
    ```
    innobackupex --apply-log --redo-only /backup/full-{DATE} --user={backupuser} --password={backuppass}
    ```
    
2. Apply the incremental backups to the full backup start with the date closest to the full backup
   ```
   innobackupex --apply-log --redo-only /backup/full-{DATE} --incremental-dir/backup/incr-{DATE} --user={backupuser} --password={backuppass}
   ```
   
3. Apply other incremental backups oldest to newest date. Stop at the required date for restoring
4. Exit and stop the container, this will shut down the MySQL database
5. The MySQL data-dir is a persistant volume mounted on the local drive, remove the contents of the data-dir
6. `rsync` or `cp` the persistant backup volume mounted on the local drive to the local data-dir.
    > **IMPORTANT!** The incremental backups are applied to the full backup, so copy the full backup only!
7. Restart the container



