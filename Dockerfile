## MySQL 5.7 with Percona Xtrabackup
 
## Pull the mysql:5.7 image, a oraclelinux:7 base, derivation of RHEL
FROM mysql/mysql-server:5.7
 
## The maintainer name and email
MAINTAINER Scott Havens <scott.havens@ars.usda.gov>

WORKDIR /home

COPY bgbackup/ /home/bgbackup/

# add the bgbackup sql that is needed to create the database and user
ADD sql/initialize_bgbackup.sql /docker-entrypoint-initdb.d 

## List all packages that we want to install
ENV RPM_PACKAGE percona-xtrabackup-24-2.4.8-1.el7.x86_64.rpm 

# Install requirement
RUN yum -y update
RUN yum -y install wget mail qpress hostname perl m4 telnet mailx sendmail sendmail-cf
RUN yum clean all

# download the Persona xtrabackup
RUN wget https://www.percona.com/downloads/XtraBackup/Percona-XtraBackup-2.4.8/binary/redhat/7/x86_64/$RPM_PACKAGE

# installing through yum trys to install the Persona server as well, so install
# by rpm without dependencies
RUN rpm -Uvh --nodeps /home/percona-xtrabackup-24-2.4.8-1.el7.x86_64.rpm
 
# Create the backup destination
RUN mkdir /backup
 
# Allow mountable backup path
VOLUME /backup
